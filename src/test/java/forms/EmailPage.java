package forms;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.logging.Logger;

public class EmailPage extends MainPage{
    private static final Logger log = Logger.getLogger(EmailPage.class.getName());
    @FindBy(css = "h3 > span[email]")
    private static WebElement emailFromLocator;
    @FindBy(css = "div > h2[class][tabindex='-1']")
    private WebElement emailTopicLocator;
    @FindBy(css = "div > div > div[dir]")
    private WebElement emailContent;
    @FindBy(css = "td > div > span > span")
    private WebElement emailToLocator;
    @FindBy(css = "div[gh='mtb'] > div > div > div[act='10']")
    private WebElement deleteEmailLocator;
    @FindBy(css = "div[gh='mtb'] > div > div >div[act='17']")
    private WebElement deleteEmailConfirmLocator;

    private final WebDriver driver;
    public EmailPage(){
        super(webDriver);
        this.driver = webDriver;
        waitForElementPresented(emailFromLocator);
    }
    public EmailPage deleteEmail(){
        log.info("Delete Email");
        deleteEmailLocator.click();
        return this;
    }
    public RemovedFolder confirmEmailDeletion(){
        log.info("Confirm email deletion");
        deleteEmailConfirmLocator.click();
        return new RemovedFolder();
    }
    public String getEmailFrom(){
        return emailFromLocator.getAttribute("email");
    }
    public String getEmailTo(){
        return emailToLocator.getAttribute("email");
    }
    public String getEmailTopic(){
        return emailTopicLocator.getText();
    }
    public String getEmailContent(){
        return emailContent.getText();
    }

}
