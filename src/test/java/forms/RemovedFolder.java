package forms;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class RemovedFolder extends MainPage {
    private final WebDriver driver;
    @FindBy(id = ":106")
    private WebElement emptyTrashLocator;
    public RemovedFolder(){
        super(webDriver);
        this.driver = webDriver;
    }
}
