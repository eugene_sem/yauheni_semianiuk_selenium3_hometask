package forms;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.logging.Logger;

public class DraftEmail extends MainPage {
    private static final Logger log = Logger.getLogger(DraftEmail.class.getName());
    @FindBy(css = "div[tabindex='1'] > div > span[email]")
    private WebElement addresseeLocator;
    @FindBy(css = "td > div > h2 > div[style]")
    private WebElement topicLocator;
    @FindBy(css = "td > div > div[role='textbox']")
    private WebElement emailContentLocator;
    @FindBy(css = "td > div > div[role='button']")
    private WebElement sendButtonLocator;
    @FindBy(css = "img[src='images/cleardot.gif'][data-tooltip-delay='800']")
    protected static WebElement closeButtonLocator;
    @FindBy(css = "td > div > div > div[tabindex='1']")
    private static WebElement deleteDraftButtonLocator;


    protected final WebDriver driver;
    public DraftEmail(){
        super(webDriver);
        this.driver = webDriver;
        waitForElementPresented(topicLocator);
    }
    public DraftEmail enterAddressee(String addressee){
        log.info("Enter Addressee: " + addressee);
        addresseeLocator.sendKeys(addressee);
        return this;
    }
    public DraftEmail enterTopic(String topic){
        log.info("Enter Topic: " + topic);
        topicLocator.sendKeys(topic);
        return this;
    }
    public DraftEmail enterEmailContent(String emailContent){
        log.info("Enter email content");
        emailContentLocator.click();
        emailContentLocator.sendKeys(emailContent);
        return this;
    }
    public DraftEmail sendMessage(){
        log.info("Send the email");
        sendButtonLocator.click();
        waitLoading();
        return this;
    }
    public DraftEmail saveAndCloseMessage(){
        log.info("Save and close the email to the draft folder");
        closeButtonLocator.click();
        return this;
    }
    public DraftEmail deleteDraft(){
        log.info("Delete draft email");
        deleteDraftButtonLocator.sendKeys(Keys.ENTER); //click() method doesn't work
        return this;
    }
    public String getAddressee(){
        return addresseeLocator.getAttribute("email");
    }
    public String getEmailTopic(){
        return topicLocator.getText();
    }
    public String getEmailContent(){
        return emailContentLocator.getText();
    }
}
