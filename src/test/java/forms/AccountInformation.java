package forms;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.logging.Logger;

public class AccountInformation extends MainPage{
    private static final Logger log = Logger.getLogger(AccountInformation.class.getName());
    @FindBy(xpath = "//a[contains(@href, 'Logout')]")
    private static WebElement logOutButtonLocator;
    @FindBy(css = "div > div > div > div[aria-hidden='false']")
    private static WebElement myAccountLocator;
    @FindBy(xpath = "//a[contains(@href, 'AddSession')]")
    private static WebElement addAnotherAccountLocator;

    private final WebDriver driver;

    public AccountInformation() {
        super(webDriver);
        this.driver = webDriver;
    }
    public EnterPasswordPage logOut(){
        log.info("Log out");
        logOutButtonLocator.click();
        return new EnterPasswordPage();
    }
    public void addAnotherAccount(){
        log.info("Add another account");
        addAnotherAccountLocator.click();
    }
}
