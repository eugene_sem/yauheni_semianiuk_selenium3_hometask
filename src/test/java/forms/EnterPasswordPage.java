package forms;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.logging.Logger;

public class EnterPasswordPage extends BaseFormTemplate {
    @FindBy(xpath = "//input[@type='password']")
    private static WebElement passwordLocator;
    @FindBy(id = "passwordNext")
    private WebElement signInButton;
    @FindBy(id = "account-chooser-link")
    private WebElement anotherAccountLocator;
    private static final Logger log = Logger.getLogger(EnterPasswordPage.class.getName());

    private final WebDriver driver;
    public EnterPasswordPage(){
        super(webDriver);
        this.driver = webDriver;
        waitForElementPresented(passwordLocator);
    }
    public EnterPasswordPage typePassword(String password){
        log.info("Type password: " + password);
        passwordLocator.sendKeys(password);
        return this;
    }
    public InboxFolder signIn(){
        log.info("Sign In to the account");
        signInButton.click();
        try{
            return new InboxFolder();
        } catch (Exception e){
           throw new IllegalStateException("Login is failed!");
        }
    }
    public ChooseAccountPage signInWithDifferentAccount(){
        log.info("Sign In with different account");
        anotherAccountLocator.click();
        return new ChooseAccountPage();
    }


}
