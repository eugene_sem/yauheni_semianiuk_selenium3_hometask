package forms;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BaseFormTemplate {

    private WebElement _locator;
    static protected WebDriver webDriver;
    protected static final java.util.logging.Logger log = java.util.logging.Logger.getLogger(EnterEmailPage.class.getName());

    public static void initialiseWebDriver(){
        log.info("Initialise WebDriver");
        System.setProperty("webdriver.gecko.driver", "src/test/java/resources/geckodriver");
        webDriver = new FirefoxDriver();
        webDriver.manage().window().maximize();
    }

    public BaseFormTemplate(WebDriver webDriver)
    {
        this.webDriver = webDriver;
        PageFactory.initElements(webDriver, this);
    }
    public static WebDriver getWebDriver(){
        return  webDriver;
    }
    public void waitForElementPresented(WebElement element){
        WebDriverWait wait = new WebDriverWait(webDriver, 60);
        wait.until(ExpectedConditions.visibilityOf((element)));
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }
}
