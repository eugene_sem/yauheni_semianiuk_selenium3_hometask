package forms;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.logging.Logger;

public class ChooseAccountPage extends BaseFormTemplate {
    private static final Logger log = Logger.getLogger(ChooseAccountPage.class.getName());
    @FindBy(id = "account-chooser-add-account")
    private static WebElement addAccountLocator;
    @FindBy(id = "edit-account-list")
    private WebElement removeAccount;
    @FindBy(css = "a[jsname='AHldd']")
    private WebElement doneButton;

    private final WebDriver driver;
    public ChooseAccountPage(){
        super(webDriver);
        this.driver = webDriver;
        waitForElementPresented(addAccountLocator);
    }
    public EnterEmailPage addAccount(){
        log.info("Add new account");
        addAccountLocator.click();
        return new EnterEmailPage();
    }
    public ChooseAccountPage editAccountList(){
        log.info("Edit account List");
        removeAccount.click();
        return this;
    }
    public EnterEmailPage removeAccount(String email){
        log.info("Remove account from the list");
        driver.findElement(By.cssSelector("button[value='"+email+"']")).click();
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOf(doneButton));
        wait.until(ExpectedConditions.elementToBeClickable(doneButton));
        doneButton.click();
        return new EnterEmailPage();
    }

}
