package forms;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.logging.Logger;


public class NewDraftEmail extends DraftEmail {
    private static final Logger log = Logger.getLogger(NewDraftEmail.class.getName());
    @FindBy(name = "to")
    private WebElement addresseeLocator;
    @FindBy(name = "subjectbox")
    private WebElement topicLocator;

    private final WebDriver driver;
    public NewDraftEmail(){
        super();
        this.driver = webDriver;
        waitForElementPresented(topicLocator);
    }
    public DraftEmail enterAddressee(String addressee) {
        log.info("Enter Addressee: " + addressee);
        addresseeLocator.sendKeys(addressee);
        return this;
    }
    public DraftEmail enterTopic(String topic) {
        log.info("Enter Topic: " + topic);
        topicLocator.sendKeys(topic);
        return this;
    }
}
