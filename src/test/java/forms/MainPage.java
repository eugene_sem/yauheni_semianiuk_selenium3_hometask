package forms;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class MainPage extends BaseFormTemplate {
    private static final Logger log = Logger.getLogger(MainPage.class.getName());
    @FindBy(css = "div[gh='cm']")
    protected WebElement writeMessageButtonLocator;
    @FindBy(xpath = "//a[contains(@tabindex, '0')][contains(@href, 'inbox')]")
    protected WebElement inboxLocator;
    @FindBy(xpath = "//a[contains(@href, 'inbox')]")
    protected WebElement goToInboxLocator;
    @FindBy(xpath = "//a[contains(@tabindex, '0')][contains(@href, 'drafts')]")
    protected WebElement draftsLocator;
    @FindBy(xpath = "//a[contains(@href, 'drafts')]")
    protected WebElement goToDraftsLocator;
    @FindBy(xpath = "//a[contains(@tabindex, '0')][contains(@href, 'sent')]")
    protected WebElement sentLocator;
    @FindBy(xpath = "//a[contains(@href, 'sent')]")
    protected  WebElement goToSentLocator;
    @FindBy(xpath = "//a[contains(@href, 'SignOutOptions')]")
    protected WebElement userIconLocator;
    @FindBy(xpath = "span[gh='mll']")
    protected WebElement expandMoreLocator;
    @FindBy(xpath = "//a[contains(@tabindex, '-1')][contains(@href, 'trash')]")
    protected WebElement trashLocator;
    @FindBy(xpath = "//a[contains(@href, 'trash')]")
    protected  WebElement goToTrashLocator;
    @FindBy(name = "q")
    protected WebElement searchBoxLocator;
    @FindBy(id = "gbqfb")
    protected WebElement searchButtonLocator;
    @FindBy(id = "reauthEmail")
    protected WebElement emailLocator;
    @FindBy(css = "span[class='v1']")
    protected WebElement loadingLocator;
    @FindBy(xpath = "//div[@title='Refresh']")
    protected WebElement refreshLocator;

    protected final WebDriver driver;

    public MainPage(WebDriver webDriver)
    {
        super(webDriver);
        this.driver = webDriver;
        waitForElementPresented(writeMessageButtonLocator);
    }
    public NewDraftEmail clickWriteMessageButton(){
        log.info("Start to create of new email");
        writeMessageButtonLocator.click();
        return new NewDraftEmail();
    }
    public InboxFolder goToInboxFolder(){
        log.info("Go to the Inbox folder");
        goToInboxLocator.click();
        return new InboxFolder();
    }
    public DraftFolder goToDraftFolder(){
        log.info("Go to the Draft folder");
        goToDraftsLocator.click();
        return new DraftFolder();
    }
    public SentFolder goToSentFolder(){
        log.info("Go to the Sent folder");
        goToSentLocator.click();
        return new SentFolder();
    }
    public RemovedFolder goToRemovedFolder(){
        log.info("Go to the Trash folder");
        expandMoreItems();
        goToTrashLocator.click();
        return new RemovedFolder();
    }

    public MainPage expandMoreItems(){
        expandMoreLocator.click();
        return this;
    }
    public boolean isMessageFound(String topic) {
        log.info("Find the email with " + topic + " subject");
        WebDriverWait wait = new WebDriverWait(driver, 15);
        try {
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[text()='" + topic + "']/..")));
            log.info("Email was found");
            return true;
        } catch (Exception e) {
            log.info("Email wasn't found");
            return false;
        }
    }

    public DraftEmail openDraftEmail(String topic){
        log.info("Open the draft email");
        driver.findElement(By.xpath(".//*[text()='" + topic + "']/..")).click();
        return new DraftEmail();
    }
    public EmailPage openEmail(String topic){
        log.info("Open the email");
        WebDriverWait wait = new WebDriverWait(webDriver, 60);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[text()='" + topic + "']/..")));
        webDriver.findElement(By.xpath(".//*[text()='" + topic + "']/..")).click();
        return new EmailPage();
    }
    public AccountInformation openAccountInfo(){
        log.info("Open Account Info");
        userIconLocator.click();
        return new AccountInformation();
    }
    public void waitLoading(){
        List<WebElement> list = new ArrayList<WebElement>();
        list.add(loadingLocator);
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.invisibilityOfAllElements(list));
    }
}
