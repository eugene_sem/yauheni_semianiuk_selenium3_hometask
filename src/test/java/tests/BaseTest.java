package tests;

import forms.BaseFormTemplate;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

/**
 * Created by e.semenyuk on 23.11.2016.
 */
public class BaseTest {
    public static WebDriver driver;
    @BeforeTest
    public void startDriver() {
        //add .exe to geckodriver to run tests on windows machine
        BaseFormTemplate.initialiseWebDriver();
        driver = BaseFormTemplate.getWebDriver();
        /*System.setProperty("webdriver.gecko.driver", "src/test/java/resources/geckodriver");
        driver = new FirefoxDriver();
        driver.manage().window().maximize();*/
        /*System.setProperty("webdriver.chrome.driver", "src/test/java/resources/chromedriver.exe");
        driver = new ChromeDriver();*/

    }
    @AfterTest
    public void stopDriver() {
        driver.close();
        driver.quit();
    }
}
