package forms;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class EnterEmailPage extends BaseFormTemplate {
    @FindBy(id = "identifierId")
    private WebElement emailLocator;
    @FindBy(id = "identifierNext")
    private WebElement nextButton;

    private final WebDriver driver;

    public EnterEmailPage(){
        super(webDriver);
        this.driver = webDriver;
        waitForElementPresented(emailLocator);
    }

    public EnterEmailPage typeUsername(String username) {
        log.info("Type username: " + username);
        emailLocator.sendKeys(username);
        return this;
    }

    public EnterPasswordPage submitEmail(){
        log.info("Submit username");
        nextButton.click();
        return new EnterPasswordPage();
    }

}
